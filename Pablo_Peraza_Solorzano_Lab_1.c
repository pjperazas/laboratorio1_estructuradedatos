// Estudiante:Pablo Peraza     Carnet:2019249721

#include <stdio.h>

struct estudiante{ //Se crea un struct para poder implementar el "tipo" estudiente en el codigo
    char nombre[25];
    int carnet;
}estudiantes[10];// se define el arreglo estudiantes con 10 campos

int guardar_info(){ // esta funcion por medio de un for permite guardar el carnet y el nombre en cada uno de los 10 estudientes que estan en el arreglo
    char nombre;
    int carnet;
    
    for(int i=0;i<10;i++){
        printf("inserte el nombre del estudiante/n");
        scanf("%s", &estudiantes[i].nombre);
        printf("inserte el carnet del estudiante/n");
        scanf("%d", &estudiantes[i].carnet);
    }
   return 0;
    
}

int preguntas(){ // esta funcion pregunta por una posicion en el arreglo, para luego comparar el carnet del estudiante en dicha posicion con uno nuevo
    int x, carnet_v;
    
    printf("Que posicion de carnet desea validar?/n");
    scanf("%d", &x);
    printf("Cual es el carnet del estudiante en la posicion anterior?/n");
    scanf("%d", &carnet_v);
    if(carnet_v!= estudiantes[x].carnet){
        printf("no es correcto");
        return 0;
    }
    printf("es correcto");
    return 0;
}
int main(){
   int x;
   
   x= guardar_info();
   
   

    return preguntas();
}