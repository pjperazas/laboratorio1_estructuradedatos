// Estudiante:Pablo Peraza     Carnet:2019249721
#include <stdio.h>
typedef struct estudiante{
    char *nombre[25];
    int  *carnet;
    struct estudiante *sig;
}est;
est *crear_lista(est *lista){
    lista = NULL;
    return lista;
}
est *agregar_estudiante(est *lista, char *nombre, int *carnet){
    est *nuevo_estudiante, *estudiante_temp;
    nuevo_estudiante = (est*)malloc(sizeof(est));
    nuevo_estudiante->nombre[25]= calloc(25,sizeof(char));
    nuevo_estudiante->carnet= calloc(11,sizeof(int));
    nuevo_estudiante->nombre[25] = nombre;
    nuevo_estudiante->carnet = carnet;
    nuevo_estudiante->sig = NULL;
    if(lista == NULL){
        lista = nuevo_estudiante;
        return lista;
    }
    else{
        estudiante_temp = lista;
        while(estudiante_temp->sig != NULL){
            estudiante_temp = estudiante_temp->sig;
        }
        estudiante_temp->sig = nuevo_estudiante;
        return lista;
    }
    return lista;
}

est *agregar_estudiante_inicio(est *lista, char *nombre, int *carnet){
    est *nuevo_estudiante, *estudiante_temp;
    nuevo_estudiante = (est*)malloc(sizeof(est));
    nuevo_estudiante->nombre[25]= calloc(25,sizeof(char));
    nuevo_estudiante->carnet= calloc(11,sizeof(int));
    nuevo_estudiante->nombre[25] = nombre;
    nuevo_estudiante->carnet = carnet;
    nuevo_estudiante->sig = lista;
    if(lista == NULL){
        lista = nuevo_estudiante;
        return lista;
    }
    else{
        estudiante_temp = lista;
        while(estudiante_temp->sig != NULL){
            estudiante_temp = estudiante_temp->sig;
        }
        estudiante_temp->sig = nuevo_estudiante;
        return lista;
    }
    return lista;
}
est *borrar(est *lista, char nombre){
    est *temp;
    temp = lista;
    if(temp->nombre == nombre){
        lista = lista->sig;
        free(temp);
    } else{ 
        while ((temp->sig)->nombre != nombre){
            temp = temp->sig;
        }
        free(temp->sig);
        temp->sig =(temp->sig)->sig;
    } 
    return lista;
}
int main(){
    int memoria;
    char nombre[25];
    int carnet,x;
    
    est *lista = crear_lista(lista);
    x = 0;
    while (x != 5){
        printf("1. insertar estudiante\n");
        printf("2. insertar estudiante al inicio de la lista\n");
        printf("3. verificar carnet\n");
        printf("4. eliminar estudiante\n");
        printf("5. salir\n");
        scanf("%d", &x);
        
        if( x==1){
            printf("inserte el nombre del estudiante\n");
            scanf("%s", &nombre);
            printf("inserte el carnet del estudiante\n");
            scanf("%d", &carnet);
            int memoria = agregar_estudiante(lista,nombre,carnet);
        }
        
        if (x == 2){
            printf("inserte el nombre del estudiante\n");
            scanf("%s", &nombre);
            printf("inserte el carnet del estudiante\n");
            scanf("%d", &carnet);
            int memoria = agregar_estudiante_inicio(lista,nombre,carnet);
        }
        
        if (x == 4){
            printf("inserte el nombre del estudianteque desea borrar\n");
            scanf("%s", &nombre);
            int memoria = borrar(lista,nombre);
            printf("si desea borrar otro estudiante inserte 1, sino inserte 0\n");
            scanf("%d", &x);
        }
    }
    
    int imp = imprimir(lista,nombre,carnet);
    return 0;
}

int imprimir(est *lista,char *nombre, int *carnet){

    while(lista!=NULL){
        printf("%s",nombre);
        printf("%d",carnet);
    }
    return 0;
}