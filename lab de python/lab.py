def main():

    x=lista_enlazada()
    k = 1
    while(k == 1):
        print("Menu")
        print("1- Agregar al inicio")
        print("2- Agregar al final")
        print("3- Imprimir")
        print("4- Tamaño de la lista")
        print("5- Borrar lista")
        print("6- Invertir")
        print("7- Buscar por indice")

        opcion = input("ingrese la opcion que desee")
        if opcion == "1":
            v = input("ingrese el valor del nodo")
            agregar_inicio(x, v)
        if opcion == "2":
            v = input("ingrese el valor del nodo")
            agregar_final(x, v)
        if opcion == "3":
            imprimir(x)
        if opcion == "4":
            print("Tamaño:",tamaño(x))
        if opcion == "5":
            limpiar(x)
        if opcion == "6":
            invertir(x)
        if opcion == "7":
            i= input("ingrese el indice del nodo")
            indice(x, i)



class Nodo:

    def __init__(self,valor=None,siguiente=None):

        self.valor = valor

        self.siguiente = siguiente

    def actualizar_valor(valor_nuevo):

        self.valor = valor_nuevo

    def actualizar_siguiente(siguiente_nuevo):

        self.siguiente = siguiente_nuevo

class lista_enlazada:

    def  __init__(self): #constructor
        self.cabeza = None

def agregar_inicio(self,valor):

    temp = Nodo(valor)

    temp.siguiente = self.cabeza

    self.cabeza = temp

    del temp


def agregar_final(self, valor):

    nuevo_nodo = Nodo(valor)
        
    if self.cabeza is None:
            
        self.cabeza = nuevo_nodo
            
        return
        
    temp = self.cabeza
        
    while temp.siguiente is not None:
            
        temp= temp.siguiente
            
    temp.siguiente = nuevo_nodo;
        
def imprimir(self):

    temp = self.cabeza

    while(temp != None):

        print(temp.valor)

        temp = temp.siguiente

    print("No hay mas elementos en la lista")

    del temp


def tamaño(self):

    temp = self.cabeza
    cont = 0
    while(temp != None):

        cont += 1
        temp = temp.siguiente

    return cont

    del temp

def limpiar(self):

    self.cabeza = None

def indice(self, indice):

    i, j = 0, self.cabeza
    
    while j != None:
            
        if i == indice:
                
            return j.valor
            
        i += 1
            
        j = j.siguiente

        return j

def invertir(self):
    
        temp = self.cabeza
        
        anterior = None
        
        while temp != None:
            
            siguiente = temp.siguiente
            
            temp.siguiente = anterior
            
            anterior = temp
            
            temp = siguiente
            
        self.cabeza = anterior

        del temp


main()
