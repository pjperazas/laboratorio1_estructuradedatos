#include <stdio.h>
#include <stdlib.h>
#include <malloc.h> 

typedef struct Nodo {
	struct Nodo* padre;
	struct Nodo* izquierda;
	struct Nodo* derecha;
	int valor;
}Nodo;

struct Nodo* nuevo_nodo(Nodo* padre, int valor);
void nuevo_valor(Nodo* nodo, int valor);

int main() {
	struct Nodo* nodo;
	nodo = nuevo_nodo(NULL, 33);
	nuevo_valor(nodo, 99);
	nuevo_valor(nodo, 44);
	nuevo_valor(nodo, 55);
	nuevo_valor(nodo, 77);
	nuevo_valor(nodo, 11);
	nuevo_valor(nodo, 00);
	return 0;
}

Nodo* nuevo_nodo(Nodo* padre, int valor) {
	Nodo* nuevo = calloc(sizeof(Nodo), 1);
	nuevo->padre = padre;
	nuevo->valor = valor;
	return nuevo;
}

void nuevo_valor(Nodo* nodo, int valor) {
	struct Nodo* temp, * pivote;
	int derecha = 0;
	if (nodo) {
		temp = nodo;
		while (temp != NULL) {
			pivote = temp;
			if (valor > temp->valor) {

				temp = temp->derecha;
				derecha = 1;
			}
			else {
				temp = temp->izquierda;
				derecha = 0;
			}
		}
		temp = nuevo_nodo(pivote, valor);
		if (derecha) {
			printf("se coloco %i al lado izquierdo de %i\n", valor, pivote->valor);
			pivote->derecha = temp;
		}
		else {
			printf("se coloco %i al lado derecho de %i\n", valor, pivote->valor);
			pivote->izquierda = temp;
		}
	}
	else {
		printf("No existe tal arbol");
	}
}