﻿//Pablo Peraza Solorzano 2019249721

/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante:
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
    	//struct nodo_estudiante *ref_anterior;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parámetros
	Salidas: no retorna nada por ser de tipo void
	Funcionamiento: inicia la lista
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: recibe una variabe que se llama nuevo y es de tipo nodo estudiante
	Salidas: no retorna nada por ser de tipo void
	Funcionamiento: inserta un nuevo nodo al inicio de la lista
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: recibe una variabe que se llama nuevo y es de tipo nodo estudiante
	Salidas: no retorna nada por ser de tipo void
	Funcionamiento: inserta un nuevo nodo al final de la lista
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: un entero llamado indice que funciona como indice
	Salidas:  no retorna nada por ser de tipo void
	Funcionamiento: utiliza el indice para seleccionar el estudiante que desea borrar mediante una comparacion
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: un entero llamado indice que funciona como indice
	Salidas: retorna un estudiante
	Funcionamiento: utiliza el indice para seleccionar el estudiante que desea buscar mediante una comparacion
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: dos enteros que corresponden a los carnetes, el primero es seleccionado por el indice y el segundo se ingres para la funcion con un input
	Salidas: no retorna nada por ser de tipo void
	Funcionamiento: compara los dos carnets para saber si son iguales, en caso de que lo sean se imprime que es correcto y en caso de que no se imprime que es incorrecto 
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No recibe parámetros
	Salidas: no retorna nada por ser de tipo void
	Funcionamiento: Genera el menu, con distintas selecciones
    crea un loop
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No recibe parámetros
	Salidas: retorna un entero 
	Funcionamiento: se encarga de llamar a todas las funciones necesarias para inicializar el programa
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);