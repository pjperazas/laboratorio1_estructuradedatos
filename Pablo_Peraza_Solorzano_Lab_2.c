// Estudiante:Pablo Peraza     Carnet:2019249721

#include <stdio.h>

typedef struct estudiante{
    char *nombre[25];
    int  *carnet;
    struct estudiante *sig;
}est;

est *crear_lista(est *lista){
    lista = NULL;
    return lista;
}

est *agregar_estudiante(est *lista, char *nombre, int *carnet){

    est *nuevo_estudiante, *estudiante_temp;
    nuevo_estudiante = (est*)malloc(sizeof(est));
    nuevo_estudiante->nombre[25]= calloc(25,sizeof(char));
    nuevo_estudiante->carnet= calloc(11,sizeof(int));
    nuevo_estudiante->nombre[25] = nombre;
    nuevo_estudiante->carnet = carnet;
    nuevo_estudiante->sig = NULL;
    if(lista == NULL){
        lista = nuevo_estudiante;
        return lista;
    }
    else{
        estudiante_temp = lista;
        while(estudiante_temp->sig != NULL){
            estudiante_temp = estudiante_temp->sig;
        }
        estudiante_temp->sig = nuevo_estudiante;
        return lista;
    }
    return lista;
}

int main(){
    int memoria;
    char nombre[25];
    int carnet,x;
    
    est *lista = crear_lista(lista);
    
    printf("si desea agregar un estudiante inserte 1 sino inserte 0\n");
    scanf("%d", &x);
    
    while (x == 1){
        printf("inserte el nombre del estudiante\n");
        scanf("%s", &nombre);
        printf("inserte el carnet del estudiante\n");
        scanf("%d", &carnet);
        int memoria = agregar_estudiante(lista,nombre,carnet);
        printf("si desea agrgar un estudiante inserte 1, sino inserte 0\n");
        scanf("%d", &x);
    }
    return 0;
}